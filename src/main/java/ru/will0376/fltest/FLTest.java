package ru.will0376.fltest;

import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.event.FMLInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPostInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;

@Mod(modid = FLTest.MOD_ID, name = FLTest.MOD_NAME, version = FLTest.VERSION)
public class FLTest {

	public static final String MOD_ID = "FLTest";
	public static final String MOD_NAME = "FLTest";
	public static final String VERSION = "1.0";

	@Mod.Instance(MOD_ID)
	public static FLTest INSTANCE;

	@Mod.EventHandler
	public void preinit(FMLPreInitializationEvent event) {

	}

	@Mod.EventHandler
	public void init(FMLInitializationEvent event) {

	}

	@Mod.EventHandler
	public void postinit(FMLPostInitializationEvent event) {

	}
}
