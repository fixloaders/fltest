package ru.will0376.fltest;

import io.github.crucible.grimoire.common.api.grimmix.Grimmix;
import io.github.crucible.grimoire.common.api.grimmix.GrimmixController;
import io.github.crucible.grimoire.common.api.grimmix.lifecycle.IConfigBuildingEvent;
import io.github.crucible.grimoire.common.api.mixin.ConfigurationType;

import java.util.ArrayList;
import java.util.List;

@Grimmix(id = "fltloader")
public class FLTController extends GrimmixController {
	List<String> modIds = new ArrayList<>();

	public FLTController() {
		setModList();
	}

	private void setModList() {
		modIds.add("botania");
	}

	@Override
	public void buildMixinConfigs(IConfigBuildingEvent event) {
		event.createBuilder("fixloader/mixins.fixloader.json")
				.mixinPackage("fixes.mixins.vanilla")
				.commonMixins("*")
				.refmap("fixloader.refmap.json")
				.verbose(true)
				.required(true)
				.build();

		for (String modid : modIds) {
			event.createBuilder("fixloader/mixins." + modid + ".json")
					.mixinPackage("fixes.mixins.mods")
					.configurationType(ConfigurationType.MOD)
					.commonMixins(String.format("%s.*", modid))
					.refmap("fixloader.refmap.json")
					.verbose(true)
					.required(false)
					.build();
		}
	}
}
