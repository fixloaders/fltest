package fixes.mixins.mods.botania;

import net.minecraft.entity.Entity;
import net.minecraft.item.ItemStack;
import net.minecraft.world.World;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;
import ru.will0376.fixloader.utils.identifiers.Fix;
import vazkii.botania.common.item.block.ItemBlockTinyPotato;

@Mixin(ItemBlockTinyPotato.class)
@Fix(modid = "botania", fixName = "TinyPotatoSpamDisabler")
public class MixinItemBlockTinyPotato {
	@Inject(require = 1, remap = false, method = {"func_77663_a", "onUpdate"}, at = @At("HEAD"), cancellable = true)
	public void inject(ItemStack stack, World world, Entity e, int t, boolean idunno, CallbackInfo ci) {
		//ci.cancel();
	}
}
