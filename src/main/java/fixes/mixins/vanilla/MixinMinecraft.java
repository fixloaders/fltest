package fixes.mixins.vanilla;

import net.minecraft.client.Minecraft;
import net.minecraft.client.main.GameConfiguration;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;
import ru.will0376.fixloader.utils.identifiers.Fix;

@Mixin(Minecraft.class)
@Fix(modid = "vanilla", fixName = "HelloWorldMinecraft!", type = Fix.ClassType.Vanilla, side = Fix.UseSide.Client)
public class MixinMinecraft {
	@Inject(method = "<init>", require = 1, remap = false, at = @At("RETURN"))
	public void inject(GameConfiguration p_i45547_1_, CallbackInfo ci) {
		System.out.println("Hello World!");
	}
}
